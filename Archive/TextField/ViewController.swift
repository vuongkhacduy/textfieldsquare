//
//  ViewController.swift
//  TextField
//
//  Created by hahalolo on 8/17/20.
//  Copyright © 2020 hahalolo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var codeTextField: HLTextField!
    @IBOutlet weak var doneButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
            self.customize()
            self.addGestures()
        }
        
        func customize() {
            self.codeTextField.count = codeTextField.count
            self.codeTextField.placeholder = codeTextField.placeholder
            self.codeTextField.textColorFocused = UIColor.brown
            self.codeTextField.refreshUI()
            
            // self.codeTextField.text = "1234"
            
            self.codeTextField.textChangeHandler = { text, completed in
            
                print(text ?? "")
            }
            
        }

        func addGestures() {
            let tap = UITapGestureRecognizer(target: self, action: #selector(onTap))
            self.view.addGestureRecognizer(tap)
        }

        // MARK: - Actions
        
        @objc func onTap() {
            let _ = self.codeTextField.resignFirstResponder()
        }
}

